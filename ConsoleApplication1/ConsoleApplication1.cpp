﻿#include <iostream>

const int N = 10;

void odd(int N, bool Odd)
{
        for (int i = Odd; i < N + 1; i = i+2)
        {
            std::cout << i << "\n";             
        }
    
}

int main()
{
    std::cout << "Even numbers:" << "\n";
    for (int i = 0; i < N + 1; i++)
    {
        if (i % 2 == 0)
        {
            std::cout << i << "\n";
        }
    }

    std::cout << "Odd numbers(function):" << "\n";
    odd(15, true);

    std::cout << "Even numbers(function):" << "\n";
    odd(15, false);
}
